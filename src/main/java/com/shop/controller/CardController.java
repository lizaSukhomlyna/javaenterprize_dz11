package com.shop.controller;

import com.shop.dtos.CardDto;
import com.shop.facade.CardFacade;
import com.shop.facade.UserFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;

@Controller
@RequestMapping(path = "/card")
public class CardController {

    @Autowired
    CardFacade cardFacade;
    @Autowired
    UserFacade userFacade;

    @PostMapping("/cardAdd")
    public String submitFormCardAdd(@ModelAttribute("card") CardDto cardDto) {
        cardFacade.cardAdd(cardDto);
        return "shop/cardAddSucces";
    }

    @RequestMapping(value = "/cardAdd", method = RequestMethod.GET)
    public String showFormCardAdd(Model model) {
        Long currentUserID = userFacade.getCurrentUserId();
        model.addAttribute("card", new CardDto());
        model.addAttribute("personId", Collections.singletonList(currentUserID));
        return "shop/cardAdd";
    }

    @GetMapping("/allCardsShow")
    public String showFormCardFind(Model model) {
        List<CardDto> cardsDto = cardFacade.getAllCards();
        model.addAttribute("cards", cardsDto);
        return "shop/cardsList";
    }

    @PostMapping("/getCost")
    public String submitFormCardAdd(Model model) {
        Long cardId = (Long) model.getAttribute("idCard");
        cardFacade.calculateCostOfCardById(cardId);
        return "shop/productsCostShow";
    }

    @RequestMapping(value = "/getCost", method = RequestMethod.GET)
    public String showFormCostFind(Model model) {
        List<Long> cardIdList = cardFacade.getAllIdCards();
        List<CardDto> cardsDto = cardFacade.getAllCards();
        model.addAttribute("cards", cardsDto);
        model.addAttribute("cardIdList", cardIdList);
        return "shop/productsCost";
    }


}
