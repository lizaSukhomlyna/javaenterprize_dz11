package com.shop.controller;

import com.shop.dtos.ProductDto;
import com.shop.entity.User;
import com.shop.facade.CardFacadeImpl;
import com.shop.facade.ProductFacade;
import com.shop.facade.UserFacade;
import com.shop.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping(path = "/product")
public class ProductController {


    @Autowired
    private ProductFacade productFacade;

    @Autowired
    private CardFacadeImpl cardFacade;


    @Autowired
    private UserFacade userFacade;


    @Autowired
    ProductService productService;


    @PostMapping("/productAdd")
    public String submitFormProductAdd(@ModelAttribute("product") ProductDto productDto) {
        productFacade.addProductByPersonId(productDto, userFacade.getCurrentUserId());
        return "shop/productAddSucces";
    }

    @GetMapping("/productAdd")
    public String showFormProductAdd(Model model) {
        List<Long> cardIdList = cardFacade.getAllIdCards();
        model.addAttribute("product", new ProductDto());
        model.addAttribute("cardIdList", cardIdList);
        return "shop/productsAdd";
    }

    @GetMapping("/allProductShow")
    public String showFormProductFind(Model model) {
        List<ProductDto> productDtos = productFacade.getProductsByPersonId(
                userFacade.getCurrentUserId());
        model.addAttribute("products", productDtos);
        return "shop/productsList";
    }

}
