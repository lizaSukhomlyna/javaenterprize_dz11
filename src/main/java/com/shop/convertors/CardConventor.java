package com.shop.convertors;

import com.shop.dtos.CardDto;
import com.shop.entity.Card;

import java.math.BigDecimal;

public class CardConventor {

    public static Card cardDtoConvertToCard(CardDto cardDto) {
        Card card = new Card();
        card.setIdCard(cardDto.getIdCard());
        card.setIdPerson(cardDto.getIdPerson());
        card.setCost(BigDecimal.ZERO);
        card.setProducts(cardDto.getProducts());
        return card;
    }

    public static CardDto cardConvertToCardDto(Card card) {
        CardDto cardDto = new CardDto();
        cardDto.setIdCard(card.getIdCard());
        cardDto.setIdPerson(card.getIdPerson());
        cardDto.setProducts(card.getProducts());
        cardDto.setCost(card.getCost());
        return cardDto;
    }
}
