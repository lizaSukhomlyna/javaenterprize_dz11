package com.shop.convertors;

import com.shop.dtos.ProductDto;
import com.shop.entity.Product;

public class ProductConventor {


    public static Product productDtoconvertToProduct(ProductDto productDto) {
        Product product = new Product();
        product.setId(productDto.getId());
        product.setPersonId(productDto.getPersonId());
        product.setCardId(productDto.getCard_id());
        product.setName(productDto.getName());
        product.setCost(productDto.getCost());
        return product;
    }

    public static ProductDto productConvertToProductDto(Product pr) {
        ProductDto productDto;
        productDto = new ProductDto();
        productDto.setId(pr.getId());
        productDto.setName(pr.getName());
        productDto.setCost(pr.getCost());
        productDto.setCard_id(pr.getCardId());
        productDto.setPersonId(pr.getPersonId());
        return productDto;
    }
}
