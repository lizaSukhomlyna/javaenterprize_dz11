package com.shop.convertors;

import com.shop.dtos.UserDto;
import com.shop.entity.User;

public class UserConventor {

    public static User userDtoconvertToUser(UserDto userDto) {
        User user = new User();
        user.setUsername(userDto.getUsername());
        user.setPassword(userDto.getPassword());
        user.setPasswordConfirm(userDto.getPasswordConfirm());
        user.setRoles(userDto.getRoles());
        user.setId(userDto.getId());
        return user;
    }

    public static UserDto userConverToUserDto(User user) {
        UserDto userDto = new UserDto();
        userDto.setUsername(user.getUsername());
        userDto.setPassword(user.getPassword());
        userDto.setPasswordConfirm(user.getPasswordConfirm());
        userDto.setRoles(user.getRoles());
        userDto.setId(user.getId());
        return userDto;
    }
}
