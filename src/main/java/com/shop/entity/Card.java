package com.shop.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Getter
@Setter
@Entity
@Table(name = "cards")
//@ToString
public class Card {

    @Id
    @Column(name = "idCard")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idCard;

    @Column(name = "idPerson")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idPerson;
    @Column(name = "cost")
    private BigDecimal cost = null;

    @Column(name = "products")
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "cardId")
    List<Product> products = new ArrayList<>();


    @Override
    public String toString() {
        return "Card{" +
                "idCard=" + idCard +
                ", idPerson=" + idPerson +
                ", cost=" + cost +
                ", products=" + products +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Card card = (Card) o;
        return Objects.equals(idCard, card.idCard) && Objects.equals(idPerson, card.idPerson) && Objects.equals(cost, card.cost) && Objects.equals(products, card.products);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idCard, idPerson, cost, products);
    }
}
