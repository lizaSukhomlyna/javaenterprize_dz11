package com.shop.facade;

import com.shop.dtos.CardDto;

import java.math.BigDecimal;
import java.util.List;

public interface CardFacade {

    CardDto cardAdd(CardDto cardDto);

    List<CardDto> getAllCards();

    List<Long> getAllIdCards();

    BigDecimal calculateCostOfCardById(Long id);
}
