package com.shop.facade;

import com.shop.convertors.CardConventor;
import com.shop.dtos.CardDto;
import com.shop.entity.Card;
import com.shop.service.CardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Component
public class CardFacadeImpl implements CardFacade {

    @Autowired
    private CardService cardService;


    @Override
    public CardDto cardAdd(CardDto cardDto) {
        Card card = cardService.createCardByPersonId(
                CardConventor.cardDtoConvertToCard(cardDto));
        CardDto cardDtoReturn = CardConventor.cardConvertToCardDto(card);
        return cardDtoReturn;
    }

    @Override
    public List<CardDto> getAllCards() {
        List<Card> cards = cardService.getAllCards();
        List<CardDto> cardsDto = new ArrayList<>();
        for (Card card : cards) {
            cardsDto.add(CardConventor.cardConvertToCardDto(card));
        }
        return cardsDto;
    }

    @Override
    public List<Long> getAllIdCards() {
        List<Long> cardIdList = cardService.getAllIdCards();
        return cardIdList;
    }

    @Override
    public BigDecimal calculateCostOfCardById(Long cardId) {
        return cardService.calculateCostOfCardById(cardId);
    }
}
