package com.shop.facade;

import com.shop.dtos.ProductDto;

import java.util.List;

public interface ProductFacade {

    ProductDto addProductByPersonId(ProductDto productDto, Long id);


    List<ProductDto> getProductsByPersonId(Long id);
}
