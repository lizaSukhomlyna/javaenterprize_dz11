package com.shop.facade;

import com.shop.convertors.ProductConventor;
import com.shop.dtos.ProductDto;
import com.shop.entity.Product;
import com.shop.service.CardService;
import com.shop.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ProductFacadeImpl implements ProductFacade {


    @Autowired
    ProductService productService;

    @Autowired
    CardService cardService;


    @Override
    public ProductDto addProductByPersonId(ProductDto productDto, Long id) {
        productDto.setPersonId(id);
        Product product = productService.addProductByPersonId(
                ProductConventor.productDtoconvertToProduct(productDto));
        cardService.calculateCostOfCardById(product.getCardId());
        return ProductConventor.productConvertToProductDto(product);
    }


    @Override
    public List<ProductDto> getProductsByPersonId(Long id) {
        List<Product> products = productService.getProductsByPersonId(id);
        List<ProductDto> productDtos = new ArrayList<>();
        ProductDto productDto;
        for (Product pr : products) {
            productDto = ProductConventor.productConvertToProductDto(pr);
            productDtos.add(productDto);
        }
        return productDtos;
    }
}
