package com.shop.facade;

import com.shop.dtos.UserDto;
import com.shop.entity.User;

import java.util.List;

public interface UserFacade {

    Long getCurrentUserId();

    List<UserDto> getAllUsers();

    void deleteUser(String action, Long id);

    List<UserDto> usergtList(Long userId);

    public boolean userIsExist(User userForm);


}
