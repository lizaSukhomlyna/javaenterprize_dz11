package com.shop.facade;

import com.shop.convertors.UserConventor;
import com.shop.dtos.UserDto;
import com.shop.entity.User;
import com.shop.repository.UserRepository;
import com.shop.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class UserFacadeImpl implements UserFacade {
    @Autowired
    UserRepository userRepository;

    @Autowired
    UserService userService;

    @Override
    public Long getCurrentUserId() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return userRepository.findByUsername(authentication.getName()).getId();
    }


    public List<UserDto> getAllUsers() {
        List<User> users = userService.allUsers();
        List<UserDto> userDtos = new ArrayList<>();
        for (User user : users) {
            userDtos.add(UserConventor.userConverToUserDto(user));
        }
        return userDtos;
    }

    public void deleteUser(String action, Long id) {
        if (action.equals("delete")) {
            userService.deleteUser(id);
        }
    }

    public List<UserDto> usergtList(Long userId) {
        List<User> users = userService.usergtList(userId);
        List<UserDto> usersDto = new ArrayList<>();
        for (User user : users) {
            usersDto.add(UserConventor.userConverToUserDto(user));

        }
        return usersDto;
    }

    public boolean userIsExist(User userForm) {
        return !userService.saveUser(userForm);
    }
}
