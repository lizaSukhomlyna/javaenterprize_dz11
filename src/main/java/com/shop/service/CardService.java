package com.shop.service;


import com.shop.entity.Card;

import java.math.BigDecimal;
import java.util.List;

public interface CardService {

    Card createCardByPersonId(Card card);


    List<Long> getAllIdCards();

    List<Card> getAllCards();

    BigDecimal calculateCostOfCardById(Long id);


}
