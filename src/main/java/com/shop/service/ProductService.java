package com.shop.service;


import com.shop.entity.Product;

import java.util.List;

public interface ProductService {

    Product addProductByPersonId(Product product);

    String getProductsByPersonIdInString(Long id);

    List<Product> getProductsByPersonId(Long id);


}
