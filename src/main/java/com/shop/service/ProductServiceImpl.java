package com.shop.service;


import com.shop.entity.Product;
import com.shop.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class ProductServiceImpl implements ProductService {
    @Autowired
    private ProductRepository productRepository;


    @Override
    public Product addProductByPersonId(Product product) {
        if (product.getPersonId() != null
                && product.getName() != null
                && product.getCardId() != null
                && product.getCost() != null) {
            productRepository.save(product);
            return product;
        } else {
            throw new IllegalArgumentException("Product don't fill");
        }
    }

    @Override
    public String getProductsByPersonIdInString(Long id) {
        try {
            return productRepository
                    .findById(id)
                    .orElseThrow(() -> new Exception("Product wasn't found" + new Product())).toString();
        } catch (Exception e) {
            return e.getMessage();
        }
    }

    @Override
    public List<Product> getProductsByPersonId(Long id) {
        List<Product> products = new ArrayList<>();
        productRepository.findAll().forEach(el ->
        {
            if (Objects.equals(el.getPersonId(), id)) {
                products.add(el);
            }
        });
        if (!products.isEmpty()) {
            return products;
        } else {
            throw new IllegalArgumentException("Dont find this product");
        }
    }
}
