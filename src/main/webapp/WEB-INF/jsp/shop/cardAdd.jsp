<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="ISO-8859-1">
    <title>User Registration Form</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Sofia">
    <link rel="stylesheet" type="text/css" href="${contextPath}/resources/css/style.css">
<body>
<div align="center">
    <h2>Add card</h2>
    <form:form action="cardAdd" method="post" modelAttribute="card">
        <h3>${pageContext.request.userPrincipal.name}</h3>
        <form:label path="idPerson">Person id:</form:label>
        <form:select path="idPerson" items="${personId}"/><br/>
        <form:button>Register</form:button>
    </form:form>
</div>
<h2>Return to pages</h2>
<p>
    <a href="../index">Return to main page</a>
    <a href="/card/cardAdd">Add card</a>
    <a href="/card/allCardsShow">Show all cards</a>
    <a href="/product/productAdd">Add product</a>
    <a href="/product/allProductShow">Show all product</a>
</p>

</body>
</html>