<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="ISO-8859-1">
    <title>Registration Success</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Sofia">
    <link rel="stylesheet" type="text/css" href="${contextPath}/resources/css/style.css">
<body>
<div align="center">
    <h2>Registration Succeeded!</h2>
    <span>You add person: </span><span>${person.firstName}</span><br/>

</div>
<h2>Return to pages</h2>
<p>
    <a href="../index">Return to main page</a>
    <a href="/card/cardAdd">Add card</a>
    <a href="/card/allCardsShow">Show all cards</a>
    <a href="/product/productAdd">Add product</a>
    <a href="/product/allProductShow">Show all product</a>
</p>
</body>
</html>