<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="UTF-8"/>
    <title>Person List</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/style.css"/>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Sofia">
    <link rel="stylesheet" type="text/css" href="${contextPath}/resources/css/style.css">

</head>
<body>
<h1>Person List</h1>

<br/><br/>
<div>
    <table border="1">
        <tr>
            <th>id</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Email</th>
        </tr>
        <c:forEach items="${persons}" var="item">
            <tr>
                <td><c:out value="${item.id}"/></td>
                <td><c:out value="${item.firstName}"/></td>
                <td><c:out value="${item.lastName}"/></td>
                <td><c:out value="${item.email}"/></td>


            </tr>
        </c:forEach>
    </table>
</div>
<h2>Return to pages</h2>
<p>
    <a href="../index">Return to main page</a>
    <a href="/card/cardAdd">Add card</a>
    <a href="/card/allCardsShow">Show all cards</a>
    <a href="/product/productAdd">Add product</a>
    <a href="/product/allProductShow">Show all product</a>
</p>
</body>

</html>