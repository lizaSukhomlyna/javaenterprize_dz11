<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
  <head>
    <meta charset="UTF-8" />
    <title>Person List</title>
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Sofia">
      <link rel="stylesheet" type="text/css" href="${contextPath}/resources/css/style.css">
  <body>
    <h1>Products List</h1>

    <br/><br/>
    <div>
      <table border="1">
        <tr>
          <th>name</th>
          <th>cost</th>
        </tr>
             <c:forEach items="${products}" var="item">
                     <tr>
                       <td><c:out value="${item.name}" /></td>
                       <td><c:out value="${item.cost}" /></td>
                     </tr>
                   </c:forEach>
      </table>
    </div>
    <h2>Return to pages</h2>
    <p>
        <a href="../index">Return to main page</a>
        <a href="/card/cardAdd">Add card</a>
        <a href="/card/allCardsShow">Show all cards</a>
        <a href="/product/productAdd">Add product</a>
        <a href="/product/allProductShow">Show all product</a>
    </p>
  </body>

</html>