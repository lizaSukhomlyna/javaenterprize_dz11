package com.shop.controller;

import com.shop.entity.Role;
import com.shop.entity.User;
import com.shop.repository.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@SpringBootTest
@AutoConfigureMockMvc
class AdminControllerViewTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    public UserRepository userRepository;


    @Test
    @WithMockUser(roles = "ADMIN")
    @Transactional
    void testUserList() throws Exception {
        mockMvc.perform(
                        get("/admin"))
                .andExpect(status().isOk())
                .andExpect(view().name("admin"));

    }

    @Test
    @WithMockUser(roles = "ADMIN")
    @Transactional
    void testDeleteUser() throws Exception {
        User user = createTestUser();
        mockMvc.perform(
                        post("/admin")
                                .param("userId", user.getId().toString())
                                .param("action", "delete"))
                .andExpect(status().is(302))
                .andExpect(view().name("redirect:/admin"));
        Assertions.assertEquals(Optional.empty(), userRepository.findById(user.getId()));
    }

    private User createTestUser() {
        User user = new User();
        Set<Role> roles = new HashSet<>();
        roles.add(new Role(1L, "ROLE_USER"));
        user.setId(1L);
        user.setUsername("user");
        user.setRoles(roles);
        user.setPassword("1");
        userRepository.save(user);
        return user;
    }
}