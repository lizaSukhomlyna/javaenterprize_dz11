package com.shop.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.shop.dtos.CardDto;
import com.shop.entity.Role;
import com.shop.entity.User;
import com.shop.repository.CardRepository;
import com.shop.repository.UserRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@SpringBootTest
@AutoConfigureMockMvc
class CardControllerViewTest {


    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;
    @Mock
    UserRepository userRepository;

    @Autowired
    CardRepository cardRepository;

    @AfterEach
    void resetDb() {
        userRepository.deleteAll();
        cardRepository.deleteAll();
    }

    @Test
    @WithMockUser(roles = "USER")
    void submitFormCardAdd() throws Exception {
        CardDto cardDto = new CardDto();
        cardDto.setIdPerson(1L);
        cardDto.setIdCard(1L);
        cardDto.setCost(new BigDecimal(0));
        cardDto.setProducts(new ArrayList<>());
        mockMvc.perform(
                        post("/card/cardAdd")
                                .sessionAttr("card",cardDto)
                                .param("idCard", "1")
                                .param("idPerson","1")
                                .content(objectMapper.writeValueAsBytes(cardDto))
                               .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(view().name("shop/cardAddSucces"))
                .andExpect(forwardedUrl("/WEB-INF/jsp/shop/cardAddSucces.jsp"));

    }


    @Test
    @WithMockUser(roles = "USER")
    void showFormCardAdd() throws Exception {
        User user=createTestUser();
        when(userRepository.findByUsername("user")).thenReturn(user);
        mockMvc.perform(
                        get("/card/cardAdd"))
                .andExpect(status().isOk())
                .andExpect(view().name("shop/cardAdd"))
                .andExpect(forwardedUrl("/WEB-INF/jsp/shop/cardAdd.jsp"));
    }

    @Test
    @WithMockUser(roles = "USER")
    void showFormCardFind() throws Exception {
        mockMvc.perform(
                        get("/card/allCardsShow"))
                .andExpect(status().isOk())
                .andExpect(view().name("shop/cardsList"))
                .andExpect(forwardedUrl("/WEB-INF/jsp/shop/cardsList.jsp"));
    }


    @Test
    @WithMockUser(roles = "USER")
    void showFormCostFind() throws Exception {
        mockMvc.perform(
                        get("/card/getCost"))
                .andExpect(status().isOk())
                .andExpect(view().name("shop/productsCost"))
                .andExpect(forwardedUrl("/WEB-INF/jsp/shop/productsCost.jsp"));
    }


    private User createTestUser() {
        User user = new User();
        Set<Role> roles = new HashSet<>();
        roles.add(new Role(1L, "ROLE_USER"));
        user.setId(1L);
        user.setUsername("user");
        user.setRoles(roles);
        user.setPassword("1");
        userRepository.save(user);
        return user;
    }



}