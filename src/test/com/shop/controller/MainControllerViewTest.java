package com.shop.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
class MainControllerViewTest {

    @Autowired
    private MockMvc mockMvc;


    @Test
    void testOpenPageIndex() throws Exception {
        mockMvc.perform(get("/"))
                .andExpect(status().isOk())
                .andExpect(view().name("index"))
                .andExpect(forwardedUrl("/WEB-INF/jsp/index.jsp"));

    }

    @Test
    @WithMockUser(roles = "USER")
    void testOpenPageIndexShop() throws Exception {

        mockMvc.perform(get("/shop/index"))
                .andExpect(status().isOk())
                .andExpect(view().name("/shop/index"))
                .andExpect(forwardedUrl("/WEB-INF/jsp//shop/index.jsp"));

    }
}