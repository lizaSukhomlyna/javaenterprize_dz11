package com.shop.controller;

import com.shop.dtos.ProductDto;
import com.shop.entity.Card;
import com.shop.entity.Product;
import com.shop.entity.Role;
import com.shop.entity.User;
import com.shop.repository.CardRepository;
import com.shop.repository.ProductRepository;
import com.shop.repository.UserRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
class ProductControllerViewTest {

    @Autowired
    private MockMvc mockMvc;


    @Mock
    private ProductRepository productRepository;

    @Mock
    private UserRepository userRepository;

    @Mock
    private CardRepository cardRepository;

    @AfterEach
    void resetDb() {
        userRepository.deleteAll();
        //productRepository.deleteAll();
    }

    @Test
    @WithMockUser(roles = "USER")
    void testSendingFormOfProductAdd() throws Exception {
        ProductDto producDto = new ProductDto();
        producDto.setCard_id(1L);
        producDto.setPersonId(1L);
        producDto.setName("product");
        producDto.setCost(new BigDecimal(0));
        User user = createTestUser();
        when(userRepository.findByUsername("user")).thenReturn(user);
        mockMvc.perform(
                        post("/product/productAdd")
                                .sessionAttr("product", producDto)
                                .param("id", "1")
                                .param("card_id", "1")
                                .param("name", "Product")
                                .param("cost", "900")
                                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(view().name("shop/productAddSucces"))
                .andExpect(forwardedUrl("/WEB-INF/jsp/shop/productAddSucces.jsp"));

    }

    @Test
    @WithMockUser(roles = "USER")
    void testShowFormProductAdd() throws Exception {
        mockMvc.perform(
                        get("/product/productAdd"))
                .andExpect(status().isOk())
                .andExpect(view().name("shop/productsAdd"))
                .andExpect(forwardedUrl("/WEB-INF/jsp/shop/productsAdd.jsp"));
    }


    @Test
    @WithMockUser(roles = "USER")
    void testAddProductByPersonId() throws Exception {
        ProductDto producDto = new ProductDto();
        producDto.setCard_id(1L);
        producDto.setPersonId(1L);
        producDto.setName("product");
        producDto.setCost(new BigDecimal(0));
        User user = createTestUser();
        when(userRepository.findByUsername("user")).thenReturn(user);
        when(cardRepository.findById(1L)).thenReturn(Optional.of(new Card()));

        mockMvc.perform(
                        post("/product/productAdd")
                                .sessionAttr("product", producDto)
                                .param("id", "1")
                                .param("card_id", "1")
                                .param("name", "Product")
                                .param("cost", "900")
                                .contentType(MediaType.APPLICATION_JSON))

                .andExpect(status().isOk())
                .andExpect(view().name("shop/productAddSucces"))
                .andExpect(forwardedUrl("/WEB-INF/jsp/shop/productAddSucces.jsp"));
    }

    private Product createTestProduct() {
        Product product = new Product();
        product.setId(1L);
        product.setCardId(1L);
        product.setName("Product");
        product.setCost(new BigDecimal(0));
        productRepository.save(product);
        return product;
    }

    private User createTestUser() {
        User user = new User();
        Set<Role> roles = new HashSet<>();
        roles.add(new Role(1L, "ROLE_USER"));
        user.setId(1L);
        user.setUsername("user");
        user.setRoles(roles);
        user.setPassword("1");
        userRepository.save(user);
        return user;
    }
}

