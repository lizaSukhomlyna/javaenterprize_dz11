package com.shop.controller;

import com.shop.entity.Role;
import com.shop.entity.User;
import com.shop.repository.UserRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import java.util.HashSet;
import java.util.Set;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
class RegistrationControllerViewTest {

    @Autowired
    private MockMvc mockMvc;


    @Autowired
    UserRepository userRepository;

    @AfterEach
    void resetDb() {
        userRepository.deleteAll();
    }

    @Test
    void registration() throws Exception {
        mockMvc.perform(
                        get("/registration"))
                .andExpect(status().isOk())
                .andExpect(view().name("registration"))
                .andExpect(forwardedUrl("/WEB-INF/jsp/registration.jsp"));
    }

    @Test
    void addUser() throws Exception {
        mockMvc.perform(
                        post("/registration")
                                .flashAttr("userForm", createTestUserWIthError()))
                .andExpect(status().isOk())
                .andExpect(view().name("registration"))
                .andExpect(forwardedUrl("/WEB-INF/jsp/registration.jsp"));

        mockMvc.perform(
                        post("/registration")
                                .flashAttr("userForm", createTestUser()))
                .andExpect(status().is(302))
                .andExpect(view().name("redirect:/"));


    }

    private User createTestUserWIthError() {
        User user = new User();
        Set<Role> roles = new HashSet<>();
        roles.add(new Role(1L, "ROLE_USER"));
        user.setId(1L);
        user.setUsername("user1");
        user.setRoles(roles);
        user.setPassword("1");
        return user;
    }

    private User createTestUser() {
        User user = new User();
        Set<Role> roles = new HashSet<>();
        roles.add(new Role(1L, "ROLE_USER"));
        user.setId(1L);
        user.setUsername("user2");
        user.setRoles(roles);
        user.setPassword("1");
        user.setPasswordConfirm("1");
        return user;
    }
}