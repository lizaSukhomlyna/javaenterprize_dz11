package com.shop.convertors;

import com.shop.dtos.CardDto;
import com.shop.entity.Card;
import com.shop.entity.Product;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

class CardConventorTest {


    @ParameterizedTest
    @MethodSource("testCasesForConvertCardDto")
    void cardDtoConvertToCard(CardDto in, Card expected) {
        Assertions.assertEquals(expected, CardConventor.cardDtoConvertToCard(in));
    }

    @ParameterizedTest
    @MethodSource("testCasesForConvertCard")
    void cardConvertToCardDto(Card in, CardDto expected) {
        Assertions.assertEquals(expected, CardConventor.cardConvertToCardDto(in));
    }

    public static Stream<Arguments> testCasesForConvertCardDto() {

        return Stream.of(
                Arguments.of(fillRightCardDtoForTest(1L), fillRightCardForTest(1L)),
                Arguments.of(fillRightCardDtoForTest(2L), fillRightCardForTest(2L)),
                Arguments.of(fillRightCardDtoForTest(3L), fillRightCardForTest(3L))
        );
    }

    public static Stream<Arguments> testCasesForConvertCard() {

        return Stream.of(
                Arguments.of(fillRightCardForTest(1L), fillRightCardDtoForTest(1L)),
                Arguments.of(fillRightCardForTest(2L), fillRightCardDtoForTest(2L)),
                Arguments.of(fillRightCardForTest(3L), fillRightCardDtoForTest(3L))
        );
    }

    private static CardDto fillRightCardDtoForTest(Long id) {
        Product product = new Product();
        product.setCardId(id);
        product.setId(id);
        product.setName("Product" + id);
        product.setCost(new BigDecimal(0));
        List<Product> productList = new ArrayList<>();
        productList.add(product);
        CardDto card = new CardDto();
        card.setIdPerson(id);
        card.setIdCard(id);
        card.setCost(new BigDecimal(0));
        card.setProducts(productList);
        return card;
    }

    private static Card fillRightCardForTest(Long id) {
        Product product = new Product();
        product.setCardId(id);
        product.setId(id);
        product.setName("Product" + id);
        product.setCost(new BigDecimal(0));
        List<Product> productList = new ArrayList<>();
        productList.add(product);
        Card card = new Card();
        card.setIdPerson(id);
        card.setIdCard(id);
        card.setCost(new BigDecimal(0));
        card.setProducts(productList);
        return card;
    }

}