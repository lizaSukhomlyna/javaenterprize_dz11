package com.shop.convertors;

import com.shop.dtos.ProductDto;
import com.shop.entity.Product;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.math.BigDecimal;
import java.util.stream.Stream;

class ProductConventorTest {

    @ParameterizedTest
    @MethodSource("testCasesForConvertProductDto")
    void productDtoconvertToProduct(ProductDto in, Product expected) {

        Assertions.assertEquals(expected,
                ProductConventor.productDtoconvertToProduct(in));


    }

    @ParameterizedTest
    @MethodSource("testCasesForConvertProduct")
    void productConverToProductDto(Product in, ProductDto expected) {
        Assertions.assertEquals(expected,
                ProductConventor.productConvertToProductDto(in));
    }

    public static Stream<Arguments> testCasesForConvertProductDto() {

        return Stream.of(
                Arguments.of(generateProductDtoForTesting(1L), generateProductForTesting(1L)),
                Arguments.of(generateProductDtoForTesting(2L), generateProductForTesting(2L)),
                Arguments.of(generateProductDtoForTesting(3L), generateProductForTesting(3L))
        );
    }

    public static Stream<Arguments> testCasesForConvertProduct() {

        return Stream.of(
                Arguments.of(generateProductForTesting(1L), generateProductDtoForTesting(1L)),
                Arguments.of(generateProductForTesting(2L), generateProductDtoForTesting(2L)),
                Arguments.of(generateProductForTesting(3L), generateProductDtoForTesting(3L))
        );
    }

    private static Product generateProductForTesting(Long id) {
        Product product = new Product();
        product.setId(id);
        product.setPersonId(id);
        product.setCardId(id);
        product.setName("Product");
        product.setCost(new BigDecimal(900));
        return product;
    }

    private static ProductDto generateProductDtoForTesting(Long id) {
        ProductDto product = new ProductDto();
        product.setId(id);
        product.setPersonId(id);
        product.setCard_id(id);
        product.setName("Product");
        product.setCost(new BigDecimal(900));
        return product;
    }
}