package com.shop.convertors;

import com.shop.dtos.ProductDto;
import com.shop.dtos.UserDto;
import com.shop.entity.Product;
import com.shop.entity.Role;
import com.shop.entity.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Stream;

class UserConventorTest {

    @ParameterizedTest
    @MethodSource("testCasesForConvertUserDto")
    void userDtoconvertToUser(UserDto in, User expected) {
        Assertions.assertEquals(expected,UserConventor.userDtoconvertToUser(in));

    }

    @ParameterizedTest
    @MethodSource("testCasesForConvertUser")
    void userConverToUserDto(User in, UserDto expected) {
        Assertions.assertEquals(expected,UserConventor.userConverToUserDto(in));

    }

    public static Stream<Arguments> testCasesForConvertUserDto() {

        return Stream.of(
                Arguments.of(generateUserDtoForTesting(1L), generateUserForTesting(1L)),
                Arguments.of(generateUserDtoForTesting(2L), generateUserForTesting(2L)),
                Arguments.of(generateUserDtoForTesting(3L), generateUserForTesting(3L))
        );
    }


    public static Stream<Arguments> testCasesForConvertUser() {

        return Stream.of(
                Arguments.of(generateUserForTesting(1L), generateUserDtoForTesting(1L)),
                Arguments.of(generateUserForTesting(2L), generateUserDtoForTesting(2L)),
                Arguments.of(generateUserForTesting(3L), generateUserDtoForTesting(3L))
        );
    }


    private static User generateUserForTesting(Long id) {
        User user = new User();
        user.setId(1L);
        user.setPassword("$2a$10$5zGdlxWwTDuMccMZAA9AAugXW5LQN1TSSXRqT2V4U.Tncql2cGNru");
        user.setUsername("User1");
        Set<Role> roles = new HashSet<>();
        roles.add(new Role(1L, "ROLE_USER"));
        user.setRoles(roles);
        return user;
    }

    private static UserDto generateUserDtoForTesting(Long id) {
        UserDto user = new UserDto();
        user.setId(1L);
        user.setPassword("$2a$10$5zGdlxWwTDuMccMZAA9AAugXW5LQN1TSSXRqT2V4U.Tncql2cGNru");
        user.setUsername("User1");
        Set<Role> roles = new HashSet<>();
        roles.add(new Role(1L, "ROLE_USER"));
        user.setRoles(roles);
        return user;
    }


}