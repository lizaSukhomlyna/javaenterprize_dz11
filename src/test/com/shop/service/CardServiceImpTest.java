package com.shop.service;

import com.shop.entity.Card;
import com.shop.entity.Product;
import com.shop.repository.CardRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static org.mockito.Mockito.when;


@SpringBootTest
class CardServiceImpTest {


    @InjectMocks
    CardServiceImp cardService;

    @MockBean
    CardRepository cardRepository;

    private AutoCloseable closeable;


    @BeforeEach
    void setUp() {
        closeable = MockitoAnnotations.openMocks(this);
        List listCards = generateListOfCardsWithProducts();
        when(cardRepository.findAll()).thenReturn(listCards);
        when(cardRepository.findById(1L)).thenReturn(Optional.ofNullable((Card) listCards.get(0)));
    }

    @ParameterizedTest
    @MethodSource("testCasesForCreateCard")
    void testCreateCardByPersonId(Card in, Card expected) {
        ArrayList<Card> cardList = new ArrayList<>();
        cardList.add(in);
        when(cardRepository.save(in)).thenReturn(in);
        Assertions.assertEquals(expected, cardService.createCardByPersonId(in));
        Assertions.assertThrows(IllegalArgumentException.class, () -> cardService.createCardByPersonId(new Card()));
    }


    @Test
    void testGetAllCards() {
        List listCards = generateListOfCardsWithProducts();
        Assertions.assertEquals(listCards.size(), cardService.getAllCards().size());

    }


    @Test
    void testCalculateCostOfCardById() {
        Assertions.assertEquals(new BigDecimal(1200),
                cardService.calculateCostOfCardById(1L));

    }


    @AfterEach
    void closeService() throws Exception {
        closeable.close();
    }

    private static Card fillRightCardForTest(Long id) {
        Product product = new Product();
        product.setCardId(id);
        product.setId(id);
        product.setName("Product" + id);
        product.setCost(new BigDecimal(0));
        List<Product> productList = new ArrayList<>();
        productList.add(product);
        Card card = new Card();
        card.setIdPerson(id);
        card.setIdCard(id);
        card.setCost(new BigDecimal(0));
        card.setProducts(productList);

        return card;
    }

    public static Stream<Arguments> testCasesForCreateCard() {
        Card firstCard = fillRightCardForTest(1L);
        Card secondCard = fillRightCardForTest(2L);
        Card thirdCard = fillRightCardForTest(1L);

        return Stream.of(
                Arguments.of(firstCard, firstCard),
                Arguments.of(secondCard, secondCard),
                Arguments.of(thirdCard, thirdCard)
        );
    }

    private static List generateListOfCardsWithProducts() {
        Card card = fillRightCardForTest(1L);
        List listCards = Arrays.asList(card);
        Product firstProduct = generateProductById(card.getIdCard(),
                card.getIdCard());
        Product secondProduct = generateProductById(card.getIdCard(),
                card.getIdCard());
        List<Product> listProduct = new ArrayList<>();
        listProduct.add(firstProduct);
        listProduct.add(secondProduct);
        card.setProducts(listProduct);
        return listCards;
    }

    private static Product generateProductById(Long idPerson, Long idCard) {
        Product product = new Product();
        product.setId(idPerson);
        product.setName("Product2");
        product.setCardId(idCard);
        product.setCost(new BigDecimal(600));
        return product;
    }

}