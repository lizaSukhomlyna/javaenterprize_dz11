package com.shop.service;

import com.shop.entity.Product;
import com.shop.repository.ProductRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.when;


class ProductServiceImplTest {

    @InjectMocks
    ProductServiceImpl productService;

    @Mock
    ProductRepository productRepository;

    private AutoCloseable closeable;


    @BeforeEach
    void setUp() {
        closeable = MockitoAnnotations.openMocks(this);
        Product product = getProductForTesting();
        List<Product> productList = new ArrayList<>();
        productList.add(product);
        when(productRepository.save(product)).thenReturn(product);
        when(productRepository.findAll()).thenReturn(List.of(product));
        when(productRepository.findById(product.getId())).thenReturn(Optional.of(product));


    }

    @AfterEach
    void closeService() throws Exception {
        closeable.close();
    }


    @Test
    void testAddProductByPersonId() {
        Product product = getProductForTesting();
        Assertions.assertEquals(product, productService.addProductByPersonId(product));
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> productService.addProductByPersonId(new Product()));
    }


    @Test
    void getProductsByPersonId() {
        Product product = getProductForTesting();
        Assertions.assertEquals(List.of(product), productService.getProductsByPersonId(product.getId()));
        Assertions.assertThrows(IllegalArgumentException.class, () -> productService.getProductsByPersonId(111L));
    }

    @Test
    void getProductsByPersonIdInString() {
        Product product = getProductForTesting();
        Assertions.assertEquals(product.toString(), productService.getProductsByPersonIdInString(product.getId()));
        Assertions.assertThrows(Exception.class, () -> productService.getProductsByPersonId(111L));

    }


    private Product getProductForTesting() {
        Product product = new Product();
        product.setId(1L);
        product.setPersonId(1L);
        product.setCardId(1L);
        product.setName("Product");
        product.setCost(new BigDecimal(900));
        return product;
    }
}