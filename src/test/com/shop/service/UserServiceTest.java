package com.shop.service;

import com.shop.entity.Role;
import com.shop.entity.User;
import com.shop.repository.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

class UserServiceTest {


    @InjectMocks
    UserService userService;

    @Mock
    UserRepository userRepository;
    @Mock
    BCryptPasswordEncoder bCryptPasswordEncoder;

    private AutoCloseable closeable;


    @BeforeEach
    void setUp() {
        closeable = MockitoAnnotations.openMocks(this);
        User user = fillUserForTest();
        List<User> userList = new ArrayList<>();
        when(userRepository.findAll()).thenReturn(userList);
        when(userRepository.findByUsername(user.getUsername())).thenReturn(user);
        when(userRepository.findById(user.getId())).thenReturn(Optional.of(user));
        when(bCryptPasswordEncoder.encode(user.getPassword())).thenReturn("$2a$12$oG25a2HR7QntTm0P.IWXM.2TU.b0JRRHuOG1FI.M7YgZltC0QVh6S");

    }


    @Test
    void testLoadUserByUsername() {
        User user = fillUserForTest();
        Assertions.assertEquals(user,
                userService.loadUserByUsername(user.getUsername()));
    }

    @Test
    void testFindUserById() {
        User user = fillUserForTest();
        Assertions.assertEquals(user,
                userService.findUserById(user.getId()));

    }

    @Test
    void testAllUsers() {
        fillUserForTest();
        List<User> userList = new ArrayList<>();
        Assertions.assertEquals(userList, userService.allUsers());
    }

    @Test
    void testSaveUser() {
        User user = fillUserForTest();
        Assertions.assertFalse(userService.saveUser(user));
        Assertions.assertTrue(userService.saveUser(generateNewUser()));
    }

    private static User generateNewUser() {
        User newUser = new User();
        newUser.setId(7L);
        newUser.setUsername("NewUser");
        newUser.setPassword("24");
        Set<Role> roles = new HashSet<>() {
        };
        Role role = new Role();
        role.setName("ROLE_NAME");
        roles.add(role);
        newUser.setRoles(roles);
        newUser.setPassword("1");
        newUser.setPasswordConfirm("1");
        return newUser;
    }

    @Test
    void testDeleteUser() {
        final User user = fillUserForTest();
        when(userRepository.findById(1L))
                .thenReturn(Optional.of(user))
                .thenReturn(null);

        final boolean result = userService.deleteUser(1L);
        assertTrue(result);
    }

    private User fillUserForTest() {
        User user = new User();
        user.setId(1L);
        user.setPassword("$2a$10$5zGdlxWwTDuMccMZAA9AAugXW5LQN1TSSXRqT2V4U.Tncql2cGNru");
        user.setUsername("User1");
        Set<Role> roles = new HashSet<>();
        roles.add(new Role(1L, "ROLE_USER"));
        user.setRoles(roles);
        return user;
    }
}